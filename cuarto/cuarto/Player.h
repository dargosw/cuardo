#pragma once
#include <string>
#include <iostream>
class Player
{
private:
	std::string name;

public:
	Player(std::string name);
	friend std::ostream& operator<<(std::ostream& outstream, const Player& player);


};