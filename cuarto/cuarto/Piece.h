#pragma once
#include <string>
#include <iostream>
class Piece
{
public:
	enum class Shape : uint8_t
	{
		Round,
		Square
	};

	enum class Body : uint8_t
	{
		Full,
		Hollow
	};

	enum class Color : uint8_t
	{
		Brown,
		White
	};

	enum class Height : uint8_t
	{
		Tall,
		Short
	};

private:
	
	Body body:1;
	Color color:1;
	Height height : 1;
	Shape shape : 1;

public:
	Piece(Shape shape,	Height height,	Body body,	Color color);

	Shape Getshape()const;
	Height Getheight()const;
	Body Getbody()const;
	Color Getcolor()const;

	friend std::ostream& operator<<(std::ostream& sout, const Piece& piece);

};
