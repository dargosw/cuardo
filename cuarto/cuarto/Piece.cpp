#include "Piece.h"

std::ostream& operator<<(std::ostream& sout, const Piece& piece)
{
    sout << static_cast<int>(piece.shape) << " " << static_cast<int>(piece.body)
        << " " << static_cast<int>(piece.color) << " " << static_cast<int>(piece.height) << "\n";
    return sout;
}

Piece::Piece(Shape shape, Height height, Body body, Color color):shape(shape),height(height),body(body),color(color)
{
    static_assert(sizeof(*this) <= 1, "this class should be 1 byte");
}

Piece::Shape Piece::Getshape() const
{
    return shape;
}

Piece::Height Piece::Getheight() const
{
    return height;
}

Piece::Body Piece::Getbody() const
{
    return body;
}

Piece::Color Piece::Getcolor() const
{
    return color;
}
