#pragma once
#include"Piece.h"
#include <array>
#include<optional>
class Board
{
private:

	static const size_t Width = 4;
	static const size_t Height = 4;
	static const size_t sizeOfBoard = Width*Height;
	std::array<std::optional<Piece>, sizeOfBoard> placesUsed;

public:
	friend std::ostream& operator<<(std::ostream& outstream, const Board& board);
	using Position = std::pair<uint8_t, uint8_t>;
	const std::optional<Piece>& operator[](const Position& pos) const;
	//std::optional<Piece>& operator[](const Position& pos) const;

};