#include "Board.h"

const std::optional<Piece>& Board::operator[](const Position& pos) const
{
	const auto [line, column] = pos;

	return placesUsed[line * Width + column];
}

//std::optional<Piece>& Board::operator[](const Position& pos)
//{
//	auto [line, column] = pos;
//
//	return piecesUsed[line * Width + column];
//}

std::ostream& operator<<(std::ostream& outstream, const Board& board)
{
	Board::Position position;
	auto& [line, column] = position;
	for (line = 0; line < Board::Height; line++)
	{
		for (column = 0; column < Board::Width; column++)
		{
			if (board[position].has_value())
			{
				outstream << board[position].value();
			}
			else
			{
				outstream << "__";

			}
			outstream << " ";
		}
		outstream << std::endl;
	}
	return outstream;
}